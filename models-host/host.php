<?php
namespace Deployer;

use Deployer\Exception\Exception;

require 'recipe/common.php';
require 'recipe/drupal8.php';
require 'recipe/cachetool.php';

// TODO : contribute to deployer and require this recipe from contrib recipes.
//require dirname(__FILE__).'/../googlechat.php';

if (file_exists(dirname(__FILE__)."/deploy.php") !== false){
  require dirname(__FILE__).'/deploy.php';
} else {
  require dirname(__FILE__).'/../default/deploy.php';
}


host('xxx.xxx.xxx.xxx') // ip ou nom de domaine valide
	->port('22') // Si port standard mettre 22
	->user('xxx') // user ssh
	->stage('review') // utilisé par deployer (develop, review ou production)
	->set('deploy_path', '/var/www/xxx/www/web') // chemin ABSOLU de la racine de l'hébergement (le parent de current)
	->set('drupal_uri', 'xxx.review.insign.fr') // url d'accès au cms
	->set('drupal_site', 'xxx') // utilisé par les commandes drush -l
	->set('bin/php', 'php7.4') // bin de php, cehmin global ou absolu
	->roles('review', 'recette') // utilisé par deployer et les outils de synchro, bien mettre prod (ou production) quand c'est le cas
	->set('hosting_brand', 'ipgarde') // nom du fournisseur du hardware
	->set('vpn_access', '') //  (vide ou type, ex. openforti, meraki etc.)
	->set('hosting_details', '') // lien vers la fiche gdrive des devops
;
