#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#

# Build front
front_work_dir="./atomic-framework"

if [ -d "$front_work_dir" ] && [ "$DRUPAL_BUILD_FRONT" == 1 ]; then
  echo "Installation de la stack node (nvm, nom etc.)"
  apt-get update -y
  apt-get install -y build-essential
  curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash 
  . /root/.bashrc
  if [ -z "$NODE_VERSION" ] ; then 
    echo "Aucune variable NODE_VERSION définie dans les settings du repository. Utilisation de 12 par défaut."
    NODE_VERSION="12"
  fi
  version_pattern='^[0-9]+$'
  nvmrc_content="$(cat .nvmrc)"
  if [[ "$nvmrc_content" =~ $version_pattern ]] ; then 
    echo "Une version de node est définie dans le fichier .nvmrc : $nvmrc_content. Utilisation de cette version."
    NODE_VERSION=$nvmrc_content
  fi
  nvm install $NODE_VERSION
  cd $front_work_dir
  if [ "$(grep 'process.env.THEME' afw.params.ts)" ]; then
    echo "Le nom du theme est défini en tant que variable dans afw.params.ts."; 
    if [ -z "$DRUPAL_THEME" ] ; then 
      echo "Aucune variable DRUPAL_THEME définie dans les settings du repository. Utilisation d'olema par défaut."
      THEME="olema"
    else
      THEME=$DRUPAL_THEME
    fi
  fi
  ci_command_exists=$(cat package.json | python3 -c "import sys, json; print(json.load(sys.stdin)['scripts']['afw_ci'])")
  if [ "$?" -gt 0 ]; then
    # build:prod:dispatch n'existe pas dans les anciens projets
    # ci_command_default="npm run setup && npm run build:prod:dispatch"
    ci_command_default="npm run setup && npm run build:prod && npm run serve:dispatch"
    echo "La commande npm run afw_ci n'existe pas. Exécution de $ci_command_default"
    rm -rf ./node_modules
    # npm install
    npm ci
    eval $ci_command_default
  else
    echo "La commande npm run ci existe, on l'exécute."
    rm -rf ./node_modules
    npm ci
    npm run afw_ci
  fi
  cd ../
else  
  echo "Le dossier $front_work_dir n'existe pas. Pas de build de la partie front."
fi
