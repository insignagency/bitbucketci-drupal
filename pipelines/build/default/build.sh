#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
set -e
set -x
if [[ ! "$0" =~ ^/ ]]; then abspath="$(pwd)/$0"; else abspath="$0"; fi
abspath=${abspath//\/.\//\/}
absdirpath=$(dirname $abspath);
pipelines_parent_dir_path=$(echo $absdirpath| rev | cut -d'/' -f 4- | rev)

# Si on est pas dans un repository de travail mais dans un repo de livraison
# les sources sont dans codebase pour préserver les éléments de CI qui sont à la racine
if [[ ! "$BITBUCKET_REPO_FULL_NAME" =~ "insignagency" ]]; then 
  pipelines_parent_dir_path="$pipelines_parent_dir_path/codebase/"
fi
echo "Path du codebase : $pipelines_parent_dir_path"
cd $pipelines_parent_dir_path/pipelines/deploy/misc/

if [ -n "$SYNC_CI_DB_FROM" ] && [ "$SYNC_CI_DB_FROM" != "nosync" ]; then
  infos="$(php get-host-info.php $SYNC_CI_DB_FROM)"
  # on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
  for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
      eval $var_exp
    fi
  done

  # On teste si le cms a déjà été déployé
  env_status=$(ssh -p $port $user@$host "if [ -L $deploy_path/current ]; then echo 'deployed'; fi")
  if [ "$env_status" != "deployed" ]; then
    echo "L'environnement distant n'a pas de symlink current : on ne synchronise pas la bdd de la CI."
  elif [ "$SYNC_CI_DB_FROM" == "nosync" ]; then
    echo "La valeur de la variable d'env SYNC_CI_DB_FROM est 'nosync', pas de synchronisation de base de données."
  else
    echo "Synchronisation de la bdd avec celle de l'environnment : $SYNC_CI_DB_FROM"
    ./sync-ci-db.sh $SYNC_CI_DB_FROM
  fi
else
  echo "La variable \$SYNC_CI_DB_FROM n'existe pas : on ne synchronise pas la bdd de la CI."
fi

cd $pipelines_parent_dir_path
./pipelines/build/default/build-front.sh
./pipelines/build/default/composer-version.sh
./pipelines/build/default/install-cms.sh
