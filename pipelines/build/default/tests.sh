#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
if [[ ! "$0" =~ ^/ ]]; then abspath="$(pwd)/$0"; else abspath="$0"; fi
. $abspath/common.sh

cd workspace-test
echo "127.0.0.1 $test_site_domain" >> /etc/hosts
if [ "$(wget --no-check-certificate -S https://$test_site_domain -o /tmp/headers.txt -O /tmp/page.html; echo $?)" -gt 0 ]; then
  docker-compose -f docker-compose-ci.yml logs apache
fi
cat /tmp/headers.txt
cat /tmp/page.html |grep "block-title"
if [ -f var/log/exception.log ]; then cat var/log/exception.log; fi
if [ -d var/report/ ]; then cat var/report/*; fi