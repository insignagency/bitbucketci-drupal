#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
set -x
set -e

cat << "EOF"
+------------------------------------------------------------+

__________.__                   __
\______   \  |__ _____    ____ |  | _______     ____   ____
 |     ___/  |  \\__  \ _/ ___\|  |/ /\__  \   / ___\_/ __ \
 |    |   |   Y  \/ __ \\  \___|    <  / __ \_/ /_/  >  ___/
 |____|   |___|  (____  /\___  >__|_ \(____  /\___  / \___  >
               \/     \/     \/     \/     \//_____/      \/

Init project - By Insign
+------------------------------------------------------------+
EOF

# Start
start=$(date +"%s")
php -r "echo ini_get('memory_limit').PHP_EOL;"
php -r "echo ini_get('max_execution_time').PHP_EOL;"

echo -e "\ncomposer install --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest"
COMPOSER_MEMORY_LIMIT=-1 COMPOSER_PROCESS_TIMEOUT=600 composer install --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest

# Since we don't havea working drupal instance here, we can't use the "drupal ckeditor_media_embed:install" command.
echo -e "\nInstall CKEditor plugins (for ckeditor_media_embed)"


if [ -f ./config/sync/ckeditor_media_embed.settings.yml ]; then
    if [ -z "$CKEDITOR_VERSION" ]; then
      echo "La variable CKEDITOR_VERSION n'existe pas, il faut la définir au niveau des repository variables"
      exit 1
    fi
    echo "Ckeditor version from repository variables : ${CKEDITOR_VERSION}";
    # Prepare directories
    mkdir -p web/libraries/ckeditor/plugins
    cd web/libraries/ckeditor/plugins

    # Get each plugins
    plugin_list="embed embedsemantic embedbase autoembed autolink notification notificationaggregator textmatch"
    echo $plugin_list | tr ' ' '\n' | while read plugin; do
    echo "Download https://download.ckeditor.com/${plugin}/releases/${plugin}_${CKEDITOR_VERSION}.zip"
    curl -s -o "${plugin}_${CKEDITOR_VERSION}.zip" "https://download.ckeditor.com/${plugin}/releases/${plugin}_${CKEDITOR_VERSION}.zip"
    unzip -qq "${plugin}_${CKEDITOR_VERSION}.zip"
    rm "${plugin}_${CKEDITOR_VERSION}.zip"
  done
else
  echo "Le fichier ./config/sync/ckeditor_media_embed.settings.yml n'existe pas"
fi



echo "+----------------------------------------+"
end=$(date +"%s")
difftimelps=$(($end-$start))
echo "$(($difftimelps / 60)) minutes $(($difftimelps % 60)) seconds elapsed time."
echo "+----------------------------------------+"
