#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#

if [ -n "$COMPOSER_VERSION" ]; then
  composer self-update $COMPOSER_VERSION
else
  composer self-update 1.9.1
fi
