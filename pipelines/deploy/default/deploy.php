<?php
/*
  Ce fichier provient du repository bitbucketci-drupal
*/
namespace Deployer;
require __DIR__ . '/tasks.php';
require __DIR__ . '/googlechat.php';

desc('Deploy release on web servers');
task(
    'deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:release',
    'deploy:artifact',
    'deploy:shared',
    'deploy:writable',
    'deploy:database:backup',
    'drupal:cron:drupal_configs',
    // For real projects.
    'drupal:maintenance:on',
    'deploy:symlink',
    'drupal:status',
    //'phackage:enable-all-features',
    // For real projects.
    'drupal:cache:rebuild',
    'drupal:updatedb',
    'drupal:config:import',
    // For real projects.
    'drupal:maintenance:off',
    'drupal:cron:auto_update',
    'cleanup',
    'success'
    ]
);

// Important to declare this event after "deploy" task.
before('deploy', 'googlechat:notify');
after('success', 'googlechat:notify:success');
after('success', 'jira:release-note');
after('success', 'mabl:deployment');
after('deploy:failed', 'googlechat:notify:failure');
