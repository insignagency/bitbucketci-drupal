<?php
/*
  Ce fichier provient du repository bitbucketci-drupal
*/
namespace Deployer;

set('default_timeout', 7200);
set('keep_releases', '4');
set('drupal_path', 'web');
set('drush_cmd', '{{bin/php}} {{release_path}}/vendor/drush/drush/drush');
set('phackage_all_features',
  implode(' ', [
    'newsletter',
    'blocs_set',
    'paragraphs_set',
    'page',
    'event',
    'press_release',
    'news',
    'entity_clone_template',
    'phackage_default_content'
  ])
);
// Variable définie au niveau des repository variables
set('googlechat_webhook', $_SERVER['GOOGLE_CHAT_WEBHOOK']);
set('googlechat_buttons',function () {
  return [
    [
      'url' => 'http://'.get('drupal_uri'),
      'text' => 'Visit '.get('drupal_uri'),
      'display_on' => ['success'],
    ],
    [
      'url' => 'https://bitbucket.org/insignagency'.$_SERVER['BITBUCKET_REPO_SLUG'].'/addon/pipelines/home#!/results/' . $_SERVER['BITBUCKET_BUILD_NUMBER'],
      'text' => 'See pipeline details',
      'display_on' => ['notify', 'success', 'failure'],
    ]
  ];
});
set('shared_files', [
  '{{drupal_path}}/sites/{{drupal_site}}/settings.env.php',
]);

set('shared_dirs', [
  '{{drupal_path}}/sites/{{drupal_site}}/files',
  '{{drupal_path}}/sites/{{drupal_site}}/private',
  '{{drupal_path}}/sites/{{drupal_site}}/tmp',
]);

set('writable_dirs', [
  '{{drupal_path}}/sites/{{drupal_site}}/files',
  '{{drupal_path}}/sites/{{drupal_site}}/private',
  '{{drupal_path}}/sites/{{drupal_site}}/tmp'
]);
set('allow_anonymous_stats', false);