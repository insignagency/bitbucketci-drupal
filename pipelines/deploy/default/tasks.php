<?php
/*
  Ce fichier provient du repository bitbucketci-drupal
*/
namespace Deployer;
use Deployer\Task\Context;  
require __DIR__ . '/sets.php';


desc('Cachetool');
task('cachetool:clear:opcache', function () {
    run("if [ ! -f {{deploy_path}}/cachetool.phar ]; then cd {{deploy_path}}/; curl -sO https://gordalina.github.io/cachetool/downloads/cachetool-4.1.1.phar && mv cachetool-4.1.1.phar cachetool.phar && chmod +x cachetool.phar ; fi");
    run("cd {{release_path}} && php {{deploy_path}}/cachetool.phar opcache:reset --fcgi={{cachetool}}");
});

desc('Setup');
task('deploy:setup', function() {
  run('ln -s {{deploy_path}}/tmp/ {{deploy_path}}/current ');
});

desc('Cron drupal configs sync');
task('drupal:cron:drupal_configs', function() {
  $BITBUCKET_BRANCH = runLocally('echo $BITBUCKET_BRANCH');
  $BITBUCKET_TAG = runLocally('echo $BITBUCKET_TAG');
  $DRUPAL_CONFIG_ENV_REF = runLocally('echo $DRUPAL_CONFIG_ENV_REF');
  $AWS_LAMBDA_API_GATEWAY_KEY = runLocally('echo $AWS_LAMBDA_API_GATEWAY_KEY');
  $INSIGN_REPOSITORY_SLUG = runLocally('echo $BITBUCKET_REPO_SLUG');
  // si il y a une valeur pour tag commencçant par v on est forcément sur la branche master, cf. bitbucket-pipelines.yml
  if($BITBUCKET_BRANCH == "" && strpos($BITBUCKET_TAG, "v") === 0){
    $BITBUCKET_BRANCH = "master";
  }
  if($BITBUCKET_BRANCH == $DRUPAL_CONFIG_ENV_REF){
    writeln("Installation du cron de gestion des drupals configs.");
    run('if [ ! -d "{{release_path}}/scripts/crons/" ]; then mkdir -p {{release_path}}/scripts/crons/; fi');
    run('cp {{release_path}}/pipelines/deploy/misc/cron-drupal-configs-sync.sh {{release_path}}/scripts/crons/');
    run('sed -E -i "s|INSIGN_REPOSITORY_SLUG|'.$INSIGN_REPOSITORY_SLUG.'|" {{release_path}}/scripts/crons/cron-drupal-configs-sync.sh');
    run('sed -E -i "s|AWS_LAMBDA_API_GATEWAY_KEY|'.$AWS_LAMBDA_API_GATEWAY_KEY.'|" {{release_path}}/scripts/crons/cron-drupal-configs-sync.sh');
    run('sed -E -i "s|DRUPAL_CONFIG_ENV_REF_VALUE|'.$DRUPAL_CONFIG_ENV_REF.'|" {{release_path}}/scripts/crons/cron-drupal-configs-sync.sh');
    # On supprime la ligne insign_drupal_configs si elle existe
    $existingCrons = run('crontab -l | grep -v "insign_drupal_configs" || exit 0');
    run('echo -e "' .$existingCrons. '\n*/15 * * * * cd {{deploy_path}}/current/ && ./scripts/crons/cron-drupal-configs-sync.sh -l {{drupal_site}} # insign_drupal_configs" | crontab -');
  } else {
    # On supprime la ligne insign_drupal_configs
    run('crontab -l | grep -v "insign_drupal_configs"  | crontab -');
    writeln("L'environnement de la branche " . $BITBUCKET_BRANCH. " n'est pas l'environnement de référence pour les configs Drupal ($DRUPAL_CONFIG_ENV_REF): pas de cron à installer.");
  }
});

desc('Cron drupal auto update');
task('drupal:cron:auto_update', function() {
  $BITBUCKET_BRANCH = runLocally('echo $BITBUCKET_BRANCH');
  $BITBUCKET_TAG = runLocally('echo $BITBUCKET_TAG');
  $DRUPAL_AUTO_UPDATE_ENV_REF = runLocally('echo $DRUPAL_AUTO_UPDATE_ENV_REF');
  $AWS_LAMBDA_API_GATEWAY_KEY = runLocally('echo $AWS_LAMBDA_API_GATEWAY_KEY');
  $INSIGN_REPOSITORY_SLUG = runLocally('echo $BITBUCKET_REPO_SLUG');
  // si il y a une valeur pour tag commencçant par v on est forcément sur la branche master, cf. bitbucket-pipelines.yml
  if($BITBUCKET_BRANCH == "" && strpos($BITBUCKET_TAG, "v") === 0){
    $BITBUCKET_BRANCH = "master";
  }
  if($BITBUCKET_BRANCH == $DRUPAL_AUTO_UPDATE_ENV_REF){
    writeln("Installation du cron de gestion des drupals configs.");
    run('if [ ! -d "{{release_path}}/scripts/crons/" ]; then mkdir -p {{release_path}}/scripts/crons/; fi');
    run('cp {{release_path}}/pipelines/deploy/misc/cron-drupal-auto-update-sync.sh {{release_path}}/scripts/crons/');
    run('sed -E -i "s|INSIGN_REPOSITORY_SLUG|'.$INSIGN_REPOSITORY_SLUG.'|" {{release_path}}/scripts/crons/cron-drupal-auto-update-sync.sh');
    run('sed -E -i "s|AWS_LAMBDA_API_GATEWAY_KEY|'.$AWS_LAMBDA_API_GATEWAY_KEY.'|" {{release_path}}/scripts/crons/cron-drupal-auto-update-sync.sh');
    run('sed -E -i "s|DRUPAL_AUTO_UPDATE_ENV_REF_VALUE|'.$DRUPAL_AUTO_UPDATE_ENV_REF.'|" {{release_path}}/scripts/crons/cron-drupal-auto-update-sync.sh');
    # On supprime la ligne insign_drupal_configs si elle existe
    $existingCrons = run('crontab -l | grep -v "insign_drupal_auto_update"');
    run('echo -e "' .$existingCrons. '\n0 2 * * * cd {{deploy_path}}/current/ && ./scripts/crons/cron-drupal-auto-update-sync.sh -l {{drupal_site}} # insign_drupal_auto_update" | crontab -');
  } else {
    # On supprime la ligne insign_drupal_auto_update
    run('crontab -l | grep -v "insign_drupal_auto_update"  | crontab -');
    writeln("L'environnement de la branche " . $BITBUCKET_BRANCH. " n'est pas l'environnement de référence pour l'auto-update' Drupal ($DRUPAL_AUTO_UPDATE_ENV_REF): pas de cron à installer.");
  }
});

desc('Make and deploy artifact via rsync');
task('deploy:artifact', function() {
  $branch_deployed = runLocally('echo $BITBUCKET_BRANCH');
  $bitbucket_tag = runLocally('echo $BITBUCKET_TAG');
  $bitbucket_repo_fullname = runLocally('echo $BITBUCKET_REPO_FULL_NAME');
  $build_path = "../../../";
  if ($branch_deployed == "" && $bitbucket_tag != ""){
    // c'est un tag, on ne peut être que suur la branche master, cf. le filtre "if" dans bitbucket-pipelines.yml section tag
    $branch_deployed = "master";
    $tag_deployed = "$bitbucket_tag";
  }
  if ($branch_deployed != "") {
    runLocally('echo '.$branch_deployed.' > '.$build_path.'.branch_deployed;');
    writeln("Ajout du flag de la branche déployée : $branch_deployed");
  }
  if ($tag_deployed != "") {
    runLocally('echo '.$tag_deployed.' > '.$build_path.'.tag_deployed;');
    writeln("Ajout du flag du tag déployé : $tag_deployed");
  }
  runLocally('tar -zcvf /tmp/artifact.tgz -C '.$build_path.' .');
  upload('/tmp/artifact.tgz','{{release_path}}/artifact.tgz');
  run('tar -zxvf {{release_path}}/artifact.tgz -C {{release_path}}/ --strip 1');
  run('rm -f {{release_path}}/artifact.tgz');
}
);

/*
* A utiliser lorsque le serveur cible n'a pas rsync
*/
desc('Make and deploy artifact via scp');
task('deploy:artifact:scp', function() {
  $branch_deployed = runLocally('echo $BITBUCKET_BRANCH');
  $bitbucket_tag = runLocally('echo $BITBUCKET_TAG');
  $bitbucket_repo_fullname = runLocally('echo $BITBUCKET_REPO_FULL_NAME');
  $build_path = "../../../";
  if ($branch_deployed == "" && $bitbucket_tag != ""){
    // c'est un tag, on ne peut être que suur la branche master, cf. le filtre "if" dans bitbucket-pipelines.yml section tag
    $branch_deployed = "master";
    $tag_deployed = "$bitbucket_tag";
  }
  if ($branch_deployed != "") {
    runLocally('echo '.$branch_deployed.' > '.$build_path.'.branch_deployed;');
    writeln("Ajout du flag de la branche déployée : $branch_deployed");
  }
  if ($tag_deployed != "") {
    runLocally('echo '.$tag_deployed.' > '.$build_path.'.tag_deployed;');
    writeln("Ajout du flag du tag déployé : $tag_deployed");
  }
   runLocally('tar -zcvf /tmp/artifact.tgz -C '.$build_path.' .');
   runLocally('scp /tmp/artifact.tgz '. Context::get()->getHost() . ':{{release_path}}');
   run('tar -zxvf {{release_path}}/artifact.tgz -C {{release_path}}/ --strip 1');
   run('rm -f {{release_path}}/artifact.tgz');
}
);

desc('Backup database');
task('deploy:database:backup', function() {
    upload('../misc/deployer-mysql-dump.sh','{{deploy_path}}/deployer-mysql-dump.sh');
    run('cd {{deploy_path}}/; chmod +x deployer-mysql-dump.sh ; ./deployer-mysql-dump.sh {{deploy_path}} {{deploy_path}}/current/ deployer-db-backup.sql.gz {{drupal_site}}');
}
)->onRoles('production', 'prod');

desc('Clean redis cache');
task('deploy:redis:flushall', function() {
    run('cd {{release_path}}; redis-cli FLUSHALL');
}
);

desc('Composer install');
task('composer:19:install', function () {
    run("if [ ! -f {{deploy_path}}/composer.phar ]; then cd {{deploy_path}}; curl https://getcomposer.org/installer | /usr/bin/php ; fi");
    run("/usr/bin/php {{deploy_path}}/composer.phar self-update 1.9.1");
    run("cd {{release_path}}/; /usr/bin/php {{deploy_path}}/composer.phar install --no-dev --no-progress --prefer-dist");
});

desc('deploy cache clear');
task('deploy:cache:clear', function () {
  run("cd {{release_path}} && {{bin/php}} -f shell/uncache.php flushall flushsystem cleanmedia massrefresh varnishcacheclean");
})->desc('Clear cache');

desc('Drupal status');
task('drupal:status', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} status 2>&1; fi");
  writeln($output);
});

desc('Drupal maintenance on');
task('drupal:maintenance:on', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{bin/php}} {{deploy_path}}/current/vendor/bin/drush -l {{drupal_site}} state-set --yes system.maintenance_mode 1 2>&1; fi");
  writeln($output);
});

desc('Drupal maintenance off');
task('drupal:maintenance:off', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} state-set --yes system.maintenance_mode 0 2>&1; fi");
  writeln($output);
});

desc('Drupal cache rebuild');
task('drupal:cache:rebuild', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} --yes cache-rebuild 2>&1; fi");
  writeln($output);
});

desc('Drypal updae db');
task('drupal:updatedb', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} --yes updatedb 2>&1; fi");
  writeln($output);
});

desc('Drupal config import');
task('drupal:config:import', function () {
  $output = run("if [ -L '{{deploy_path}}/current' ] && [ -f {{deploy_path}}/current/vendor/bin/drush ]; then cd {{deploy_path}}/current && {{drush_cmd}} -l {{drupal_site}} --no-interaction config-import 2>&1; fi");
  writeln($output);
});

desc('Test');
task('deploye:test', function () {
    run("ls somethingnotexisting");
});

desc('Package enable all features');
task('phackage:enable-all-features', function () {
  $output = run(
    "if [ -L '{{deploy_path}}/current' ]; then ".
    "cd {{deploy_path}}/current;".
    "{{drush_cmd}} en {{phackage_all_features}} -l {{drupal_site}} -y 2>&1;".
    "fi"
  );
  writeln($output);
});

desc('Mabl');
task('mabl:deployment', function () {
  $MABL_API_KEY = runLocally('echo $MABL_API_KEY');
  $MABL_APP_ID = runLocally('echo $MABL_APP_ID');
  $MABL_ENV_ID = runLocally('echo $MABL_ENV_ID');
  $output = run(
    "cd {{deploy_path}}/current; export MABL_API_KEY=".$MABL_API_KEY."; export MABL_APP_ID=".$MABL_APP_ID."; export MABL_ENV_ID=".$MABL_ENV_ID."; ./pipelines/deploy/misc/mabl.sh"
  );
  writeln($output);
});

desc('Jira release note');
task('jira:release-note', function () {
  $JIRA_API_USER = runLocally('echo $JIRA_API_USER');
  $JIRA_API_TOKEN = runLocally('echo $JIRA_API_TOKEN');
  $JIRA_VERSION_TAG = runLocally('echo $BITBUCKET_TAG');
  if ($JIRA_VERSION_TAG != ""){
    $output = run(
      "cd {{deploy_path}}/current; export JIRA_API_USER=".$JIRA_API_USER."; export JIRA_API_TOKEN=".$JIRA_API_TOKEN."; export JIRA_VERSION_TAG=".$JIRA_VERSION_TAG."; ./pipelines/deploy/misc/jira-release-note.sh"
    );
    writeln($output);
    }
});


desc('Suuppression des anciennes releases : specific à drupal');
task('cleanup', function () {
    $releases = get('releases_list');
    $keep = get('keep_releases');
    $runOpts = [];

    if ($keep === -1) {
        // Keep unlimited releases.
        return;
    }

    while ($keep > 0) {
        array_shift($releases);
        --$keep;
    }

    foreach ($releases as $release) {
        $output = run("if ! which setfacl >/dev/null; then echo 'Use chmod -R 775 onto release $release'; chmod -R 775 {{deploy_path}}/releases/$release; else echo 'Use setfacl -R -m u:".Context::get()->getHost()->getUser().":rwx onto release $release'; setfacl -R -m u:".Context::get()->getHost()->getUser().":rwx {{deploy_path}}/releases/$release; 
         fi");
        writeln($output);
        //run("chmod -R 775 {{deploy_path}}/releases/$release", $runOpts);
        $output = run("rm -rf {{deploy_path}}/releases/$release", $runOpts);
        writeln($output);
    }

    run("cd {{deploy_path}} && if [ -e release ]; then rm release; fi", $runOpts);
});