#!/usr/bin/env bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
set -e

source ~/.bashrc




# Push Gitlab branches to Platform.sh environments.
# -------------------------------------------------
#
# This script can be configured by specifying environment variables in your
# repository settings or in the .gitlab-ci.yml file:
#
# variables:
#
#     # The project ID (required).
#     PF_PROJECT_ID: abcdefg123456

# Config.
if [ -z "$PF_PROJECT_ID" ]; then
  echo "PF_PROJECT_ID is required"
  exit 1
fi
if [ -z "PLATFORMSH_CLI_TOKEN" ]; then
  echo "PLATFORMSH_CLI_TOKEN is required"
  exit 1
fi

ALLOW_MASTER=${ALLOW_MASTER:-1}

export PLATFORMSH_CLI_NO_INTERACTION=1



if [ -z "$BITBUCKET_BRANCH" ]; then
  echo "Source branch (BITBUCKET_BRANCH) not defined."
  exit 1
fi
if [ -n "$BITBUCKET_TAG" ]; then
  BRANCH="master"
else
  BRANCH=$BITBUCKET_BRANCH
fi


# This script is not for production deployments.
if [ "$BRANCH" = "master" ] && [ "$ALLOW_MASTER" != 1 ]; then
  echo "Not pushing master branch."
  exit
fi

# Parent environment
if [ "$BRANCH" = "master" ] || [ "$BRANCH" = "staging" ]; then
 PF_PARENT_ENV="master"
elif [ "$BRANCH" = "review" ]; then
 PF_PARENT_ENV="staging"
elif [ "$BRANCH" = "develop" ]; then
 PF_PARENT_ENV="review"
else
 PF_PARENT_ENV="develop"
fi

echo "Executing : platform auth:info"
platform auth:info

# Set the project for further CLI commands.
echo "Executing : platform project:set-remote \"$PF_PROJECT_ID\""
platform project:set-remote "$PF_PROJECT_ID"

# Get a URL to the web UI for this environment, before pushing.
echo "Executing : platform web --environment=\"$BRANCH\" --pipe"
pf_ui=$(platform web --environment="$BRANCH" --pipe)
echo ""
echo "Web UI: ${pf_ui}"
echo ""

## Build the push command.
#push_command="platform push -vvv --force --target=${BRANCH} --activate"
## Montage des environnements spécifiques à la main, seul précision du parent develop nécessaire
#if [ "$PF_PARENT_ENV" != "$BRANCH" ] && [ "$PF_PARENT_ENV" == "develop" ]; then
#  push_command="$push_command --parent=${PF_PARENT_ENV}"
#fi

# Resolve bug rejected remote ?
# https://community.atlassian.com/t5/Bitbucket-questions/Bitbucket-pipeline-to-Heroku-fails-quot-Push-rejected-source/qaq-p/896046
#echo "git filter-branch -- --all"
#git filter-branch -- --all

#test
#if [ "$BRANCH" == "NO-ISSUE-CI-2" ]; then
  #echo "git filter-branch -- --all"
  #git filter-branch -- --all
  echo -e "\n--TEST--"
  echo -e "\nBITBUCKET_COMMIT: $BITBUCKET_COMMIT"
  echo -e "\nBITBUCKET_REPO_FULL_NAME: $BITBUCKET_REPO_FULL_NAME"
  echo -e "\nBITBUCKET_GIT_HTTP_ORIGIN: $BITBUCKET_GIT_HTTP_ORIGIN"
  echo -e "\nBITBUCKET_GIT_SSH_ORIGIN: $BITBUCKET_GIT_SSH_ORIGIN"
  echo -e "\nBITBUCKET_PROJECT_KEY: $BITBUCKET_PROJECT_KEY"
  echo -e "\nBITBUCKET_PROJECT_UUID: $BITBUCKET_PROJECT_UUID"
  echo -e "\nBITBUCKET_PIPELINE_UUID: $BITBUCKET_PIPELINE_UUID"
  echo -e "\nPLATFORM_URL: $(git config --get remote.platform.url)"
  echo -e "\n cat .git/config\n"
  cat .git/config
  echo -e "\n pwd\n"
  pwd
  echo -e "\n ls -la\n"
  ls -la

  AUTHOR_EMAIL_COMMIT=$(git log -1 --pretty=format:'%ae')

  echo -e "\n platform ssh-cert:load\n"
  platform ssh-cert:load

  # On prepare le repo PSH pour faire un commit d'update
  PLATFORM_URL=$(git config --get remote.platform.url)
  if [ "$(git ls-remote --exit-code --heads $PLATFORM_URL $BRANCH)" ]; then
    git clone -b $BRANCH $PLATFORM_URL plaform_repo
  else
    git clone -b $PF_PARENT_ENV  $PLATFORM_URL plaform_repo
    git -C plaform_repo checkout -b $BRANCH
  fi

  # On remplace temporairement le dossier.git pour push l'état local
  mv .git .gitold
  mv plaform_repo/.git .git
  git config --global user.email $AUTHOR_EMAIL_COMMIT
  git config --global user.name $AUTHOR_EMAIL_COMMIT
  cp pipelines/deploy/misc/cron-drupal-configs-sync.sh scripts/psh/
  git add -- . ':!plaform_repo' ':!.gitold' ':!env_bitbucket' ':!.bitbucket' ':!pipelines' ':!bitbucket-pipelines.yml'
  echo -e "\n git status before psh commit\n"
  git status
  git commit -m "Autocommit from $BITBUCKET_REPO_FULL_NAME" -m "[commit] $BITBUCKET_COMMIT" -m "[pipeline] $BITBUCKET_PIPELINE_UUID"
  echo -e "\n push\n"
  git push -u origin $BRANCH

  # On remet en état
  rm -rf .git plaform_repo
  mv .gitold .git

  # On active l'env
  # Build the activate command.
  activate_command="platform environment:activate $BRANCH"
  # Montage des environnements spécifiques à la main, seul précision du parent develop nécessaire
  if [ "$PF_PARENT_ENV" != "$BRANCH" ] && [ "$PF_PARENT_ENV" == "develop" ]; then
    activate_command="$activate_command --parent=${PF_PARENT_ENV}"
  fi

  # If branch is not already an activated environement, activate it
  if [ ! "$(platform environments -I --format=plain |sed '1d' | cut -f1 |grep $BRANCH)" ]; then
    echo "Executing : $activate_command"
    $activate_command
  else
    echo "Nothing to do, branch $BRANCH already activated."
  fi

#  exit
#fi

## Run the push command.
#echo "Executing : $push_command"
#$push_command

# Tansfer some bitbucket variables to platform.sh project variable to dynamically change value in file cron-drupal-configs-sync.sh
# during the step scripts/psh/deploy.sh
echo "Executing platform variable:get --level project env:AWS_LAMBDA_API_GATEWAY_KEY"
if [ ! "$(platform variable:get --level project env:AWS_LAMBDA_API_GATEWAY_KEY)" ]; then
  platform variable:create --level project --visible-build true --json=0 --sensitive=1 --name env:AWS_LAMBDA_API_GATEWAY_KEY --value $AWS_LAMBDA_API_GATEWAY_KEY
else
  platform variable:update env:AWS_LAMBDA_API_GATEWAY_KEY --value $AWS_LAMBDA_API_GATEWAY_KEY
fi
if [ ! "$(platform variable:get --level project env:INSIGN_REPOSITORY_SLUG)" ]; then
  platform variable:create --level project --visible-build true --json=0 --sensitive=1 --name env:INSIGN_REPOSITORY_SLUG --value $BITBUCKET_REPO_SLUG
else
  platform variable:update env:INSIGN_REPOSITORY_SLUG --value $BITBUCKET_REPO_SLUG
fi
if [ ! "$(platform variable:get --level project env:DRUPAL_CONFIG_ENV_REF)" ]; then
  platform variable:create --level project --visible-build true --json=0 --sensitive=1 --name env:DRUPAL_CONFIG_ENV_REF --value $DRUPAL_CONFIG_ENV_REF
else
  platform variable:update env:DRUPAL_CONFIG_ENV_REF --value $DRUPAL_CONFIG_ENV_REF
fi

# Write the environment's primary URL to a dotenv file.
# This can be used by a GitLab job via the "dotenv" artifact type.
echo "Executing : platform url --primary --pipe --yes --environment=${BRANCH}"
echo "export PRIMARY_URL=$(platform url --primary --pipe --yes --environment=${BRANCH})" > environment.env
echo "Executing : platform ssh --environment=${BRANCH} 'printenv MABL_API_KEY' | head -n 1"
echo "export MABL_API_KEY=$(platform ssh --environment=${BRANCH} 'printenv MABL_API_KEY' | head -n 1)" >> environment.env
echo "Executing : platform ssh --environment=${BRANCH} 'printenv MABL_APP_ID' | head -n 1"
echo "export MABL_APP_ID=$(platform ssh --environment=${BRANCH} 'printenv MABL_APP_ID' | head -n 1)" >> environment.env
echo "Executing : platform ssh --environment=${BRANCH} 'printenv MABL_ENV_ID' | head -n 1"
echo "export MABL_ENV_ID=$(platform ssh --environment=${BRANCH} 'printenv MABL_ENV_ID' | head -n 1)" >> environment.env

# $BITBUCKET_BRANCH
#
if [ -z "$GOOGLE_CHAT_WEBHOOK" ]; then
  echo "No var GOOGLE_CHAT_WEBHOOK"
else
  # todo: a passer dans l'image
  apt-get -y install jq
  URL_GCHAT="${GOOGLE_CHAT_WEBHOOK}"
  # Si Chat projet multithread, ajouter dans la var bitbucket "&threadKey=pipeline"
  PRIMARY_URL=$(platform url --primary --pipe --yes --environment=${BRANCH})
  AUTHOR_EMAIL_COMMIT=$(git log -1 --pretty=format:'%ae')
  RESUME_COMMIT=$(git log -1 --pretty=oneline)
  TEXT_GCHAT="*[<${PRIMARY_URL}|${BITBUCKET_BRANCH}${BITBUCKET_TAG}>]* ${AUTHOR_EMAIL_COMMIT} - ${RESUME_COMMIT}"
  JSON_POST=$(jq --null-input \
    --arg text "$TEXT_GCHAT" \
    '.text=$text')

  curl -X POST \
    -H 'Content-Type: application/json' \
    "$URL_GCHAT" \
    -d "$JSON_POST"
fi

if [ -n "$MABL_APP_ID" ]; then
    ./pipelines/deploy/misc/mabl.sh
fi

if [ -n "$BITBUCKET_TAG" ]; then
    ./pipelines/deploy/misc/jira-release-note.sh
fi

# Clean up already merged and inactive environments.
#platform environment:delete --inactive --merged --environment="$PF_PARENT_ENV" --exclude=master --exclude=staging --exclude=review --exclude=develop --exclude="$BRANCH" --yes --delete-branch --no-wait || true
