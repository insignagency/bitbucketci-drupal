#!/usr/bin/env bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
#  Script exécuté côté bitbucket, déclenché par un step appelé par cron-drupal-configs-sync.sh qui lui est exécuté sur l'environnement cible via cron
#

. /pipelines/common.sh

# !!! BOUCHON POUR TESTS local A COMMENTER AVANT PUSH
# BITBUCKET_BRANCH="masters"
# /!!!

echo "Recherche du deploy correspondant à la branche courante"
stages_found=()
for deploy_env in $(ls -d ../*/ | grep -v "misc" | sed -E "s|../||" | sed -E "s|/||"); do
  if [ "$deploy_env" == "default" ]; then continue; fi
  if [ ! -f "../$deploy_env/host.php" ]; then
    track "Problème : pipelines/deploy/$deploy_env ne contient pas de fichier host.php"
    exit 1
  fi
  stage=""
  infos=$(php get-host-info.php $deploy_env)
  if [ "$?" -gt 0 ]; then
    track "Problème sur "$deploy_env" : $infos"
    exit 1
  fi
  for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
      eval $var_exp
    fi
  done
  if [ "$stage" == "" ]; then
    track "Aucune propriété 'stage' n'est définie dans deploy/$deploy_env/host.php."
    exit 1
  else
    stages_found+=($stage)
    if [ "$stage" == "$BITBUCKET_BRANCH" ]; then
      env_to_check=$deploy_env
    elif [ "$BITBUCKET_BRANCH" == "master" ]; then
      if [ "$stage" == "production" ] || [ "$stage" == "prod" ]; then
        env_to_check="$stage"
      fi
    fi
  fi
done

# S'il y a des doublons sur la valeur stage, on avertit et abandonne.
seen=()
for i in "${stages_found[@]}"; do
  if [[ " ${seen[*]} " =~ " ${i} " ]]; then
    echo "'$i' apparaît plusieurs fois comme valeur de la propriété 'stage' dans les fichiers deploy/*/host.php."
    exit 1
  else
    seen+=($i)
  fi
done

if [ -z "$env_to_check" ]; then
  echo "Aucun deploy/*/host.php n'a de propriété 'stage' indiquant la branche courante ($BITBUCKET_BRANCH)"
  exit 1
fi

echo "Deploy trouvé : deploy/$env_to_check/host.php"

infos="$(php get-host-info.php $env_to_check)"
if [ "$?" -gt 0 ]; then
  track "Problème sur "$deploy_env" :$infos"
  exit 1
fi
for var_exp in $infos; do
  if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done
if [ "$port" == "" ]; then
  port="22"
fi
src_host="$host"
src_port="$port"
src_user="$user"
src_deploy_path="$deploy_path"
src_drupal_site="$drupal_site"

ssh_co="$src_user@$src_host"
# Ajout du port dans les configs ssh pour simplifier les chaînes de connexions
echo "Host $src_host" >>~/.ssh/config
echo "  Port $src_port" >>~/.ssh/config

# Si on est sur bitbucket
if [ ! -z "$BITBUCKET_GIT_HTTP_ORIGIN" ]; then
  # Pour atteindre le repo à l'intérieur d'un pipe http://host.docker.internal:29418/
  # Pour atteindre le repo à l'intérieur d'une pipeline http://localhost:29418/
  git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://localhost:29418/
fi

cd ../../../
if [ $src_drupal_site != "" ]; then
  sites=($src_drupal_site)
else
  sites="$(ls -d -1 ./web/sites/*/ | cut -d "/" -f4)"
fi
# on récupère le répertoires de config défini dans chaque site
configs_all=()
for site in ${sites[@]}; do
  echo "Export des configs sur $ssh_co, exécution de la commande : "
  echo "cd $src_deploy_path/current/ && vendor/drush/drush/drush -l $site core-status --field=config-sync"
  resp=$(ssh $ssh_co "cd $src_deploy_path/current/ && vendor/drush/drush/drush -l $site core-status --field=config-sync")
  if [ "$?" -gt 0 ]; then
    echo "La récupération des configs du site $site a retourné une erreur."
    echo "On passe au site suivant"
    continue
  fi
  if [ "$resp" != "" ]; then
    echo "Exécution de la commande : "
    echo "cd $src_deploy_path/current/ && vendor/drush/drush/drush -l $site cex"
    ssh $ssh_co "cd $src_deploy_path/current/ && vendor/drush/drush/drush -l $site cex"
    configs_all+=("web/"$resp)
  fi
done
# on dédoublonne le tableasu
configs=$(printf "%s\n" "${configs_all[@]}" | sort -u)
#echo "Debug $(IFS=" "; printf "%s" "${configs[*]}")"; exit 1
# on récupère le répertoires de config défini dans chaque site et les répertoires de config_split définis dans chaque config_split.config_split*
sync_made=""
for config in ${configs[@]}; do
  syncit="ok"
  if [ ! -d "$config" ]; then
    echo "Le répertoire de config $config n'existe pas, il va être synchronisé avec celui de $1".
    mkdir -p $config
  fi
  if [ "$syncit" == "ok" ]; then
    config=$(echo $config | sed -E "s|/$||")
    if [ "$config" != "" ]; then
      rsync -r --delete-after $src_user@$src_host:$src_deploy_path/current/$config/ ./$config/
      #rsync -r $src_user@$src_host:$src_deploy_path/current/$config/ ./$config/
      sync_made=$sync_made"$config "
    fi
  fi
done

do_commit=0
for config in ${configs[@]}; do
  config=$(echo $config | sed -E "s|/$||")
  if [ "$config" != "" ]; then
    modified_files=$(git status -s $config/.)
    if [ "$(echo "$modified_files" | grep -E '^[A ]?M')" ] || [ "$(echo "$modified_files" | grep -E '^[?]')" ] || [ "$(echo "$modified_files" | grep -E '^ D')" ]; then
      echo "Configs à mettre à jour"
      do_commit=1
      git add $config/.
    else
      echo "Les configs Drupal de l'environnement de référence et celles de la branche courante sont identiques."
      echo "La commande git status -s $config/. a retourné : $modified_files"
    fi
  fi
done

if ((do_commit)); then
  git commit -m "Autocommit from bitbucket-bot"
  git push
fi
