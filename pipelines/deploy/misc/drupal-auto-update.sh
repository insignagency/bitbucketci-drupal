#!/usr/bin/env bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
#  Script exécuté côté bitbucket, déclenché par un step appelé par cron-drupal-auto-update-sync.sh qui lui est exécuté sur l'environnement cible via cron
#

echo "Recherche du deploy correspondant à la branche courante pour récupérer le drupal_site"
stages_found=()
for deploy_env in $(ls -d ../*/ | grep -v "misc" | sed -E "s|../||" | sed -E "s|/||"); do
  if [ "$deploy_env" == "default" ]; then continue; fi
  if [ ! -f "../$deploy_env/host.php" ]; then
    track "Problème : pipelines/deploy/$deploy_env ne contient pas de fichier host.php"
    exit 1
  fi
  stage=""
  infos=$(php get-host-info.php $deploy_env)
  if [ "$?" -gt 0 ]; then
    track "Problème sur "$deploy_env" : $infos"
    exit 1
  fi
  for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
      eval $var_exp
    fi
  done
  if [ "$stage" == "" ]; then
    track "Aucune propriété 'stage' n'est définie dans deploy/$deploy_env/host.php."
    exit 1
  else
    stages_found+=($stage)
    if [ "$stage" == "$BITBUCKET_BRANCH" ]; then
      env_to_check=$deploy_env
    elif [ "$BITBUCKET_BRANCH" == "master" ]; then
      if [ "$stage" == "production" ] || [ "$stage" == "prod" ]; then
        env_to_check="$stage"
      fi
    fi
  fi
done

# S'il y a des doublons sur la valeur stage, on avertit et abandonne.
seen=()
for i in "${stages_found[@]}"; do
  if [[ " ${seen[*]} " =~ " ${i} " ]]; then
    echo "'$i' apparaît plusieurs fois comme valeur de la propriété 'stage' dans les fichiers deploy/*/host.php."
    exit 1
  else
    seen+=($i)
  fi
done

if [ -z "$env_to_check" ]; then
  echo "Aucun deploy/*/host.php n'a de propriété 'stage' indiquant la branche courante ($BITBUCKET_BRANCH)"
  echo "On prend la dernière valeur drupal_site trouvée : $drupal_site"
else
  echo "Deploy trouvé : deploy/$env_to_check/host.php"
  infos="$(php get-host-info.php $env_to_check)"
  if [ "$?" -gt 0 ]; then
    track "Problème sur "$deploy_env" :$infos"
    exit 1
  fi
  for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
      eval $var_exp
    fi
  done
fi

./drupal-settings-mysql.sh ../../../web/sites/$drupal_site/settings.local.php
./sync-ci-db.sh $SYNC_CI_DB_FROM
cd ../../../
. /pipelines/common.sh
# Si on est sur bitbucket
if [ ! -z "$BITBUCKET_GIT_HTTP_ORIGIN" ]; then
  # Pour atteindre le repo à l'intérieur d'un pipe http://host.docker.internal:29418/
  # Pour atteindre le repo à l'intérieur d'une pipeline http://localhost:29418/
  git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://localhost:29418/
fi

# récupérer la bdd de prod
# faire un build CI habituel
# lancer drush banana_devops:runUpdate
# commiter les modifs du code source dans une branche spécifique
echo "Version $version"
update_branch_name="autoupdate/drupal-core-$version"

if [ "$(git show-branch remotes/origin/$update_branch_name > /dev/null 2>&1)" ]; then
  echo "la branche $update_branch_name existe déjà, on arrête là."
  exit 1
fi
echo "Création et positionnement dans la branche $update_branch_name."
git checkout -b $update_branch_name
echo "Exécution de./vendor/bin/drush banana_devops:runUpdate."
drush_ps=$(./vendor/bin/drush --debug banana_devops:runUpdate -l $drupal_site > /dev/stdout 2>&1)
if [ "$?" -ne 0 ]; then
  echo "Erreur drush : $drush_ps"
elif [[ "$drush_ps" =~ [0-9] ]]; then
  echo "La mise à jour Drupal a été lancée ( devops:runUpdate)."
fi

do_commit=0
modified_files=$(git status . -- ':!env_bitbucket' ':!pipelines/*')
if [ "$(echo "$modified_files" | grep -E 'modified')" ] || [ "$(echo "$modified_files" | grep -E '^[A ]?M')" ] || [ "$(echo "$modified_files" | grep -E '^[?]')" ] || [ "$(echo "$modified_files" | grep -E '^ D')" ]; then
  echo "Des fichiers ont été modifiés. On les ajoute au référenciel git."
  do_commit=1
  git diff; exit 0
  git add . -- ':!env_bitbucket' ':!pipelines/*'
else
  echo "Aucun fichier modifié."
  echo "La commande git status a retourné : $modified_files"
fi
git diff
exit 0
# WORK IN PROGRESS DO NOT EXECUTE
if ((do_commit)); then
  git commit -m "Autocommit from bitbucket-bot from drupal auto update version : $version."
  git push -u origin $update_branch_name
fi
