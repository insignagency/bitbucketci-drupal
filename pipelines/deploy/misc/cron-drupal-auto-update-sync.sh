#!/usr/bin/env bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
#  Script exécuté sur l'environnement cible via cron. 
#
### OPTIONS ###
while getopts l: opt; do
  case "$opt" in
  l) drush_options="-l $OPTARG"; ;; \?) exit 1 ;; esac
done
shift $(($OPTIND - 1))
### OPTIONS ###

this_filename=$(basename "$0")
proc_infos=$(ps -eo pid,etime,cmd | grep $this_filename | grep -v "grep $this_filename")
etime=$(echo $proc_infos |cut -d " " -f2)
dayhour=$(echo $etime | cut -d ":" -f1)
day=$(echo $dayhour | cut -d "-" -f1)
hour=$(echo $dayhour | cut -d "-" -f2)
min=$(echo $etime | cut -d ":" -f2)
sec=$(echo $etime | cut -d ":" -f3)
#echo $proc_infos
#echo "D $day - H $hour - M $min - S "$sec
if [ $min -gt 5 ] || [  $hour -gt 0 ] || [  $day -gt 0 ]; then
  echo "Un processus tourne déjà et depuis trop longtemps, on le kill."
  JSON_POST="{'text': 'Problème du script $this_filename déployé par le repository INSIGN_REPOSITORY_SLUG sur $(hostname)'}"
  curl -X POST \
  -H 'Content-Type: application/json' \
  "https://chat.googleapis.com/v1/spaces/AAAA52lrNVM/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=wRqNDdo70gXmBrLW3V0b6ty2IXsjoq_qgUvL-19y2FI%3D" \
  -d "$JSON_POST"

  killall $this_filename
  exit 1
fi

DRUPAL_AUTO_UPDATE_ENV_REF=DRUPAL_AUTO_UPDATE_ENV_REF_VALUE
if [ ! -z "$PLATFORM_BRANCH" ]; then
  ENV_BRANCH="$PLATFORM_BRANCH"
elif [ -f ./.branch_deployed ]; then
  ENV_BRANCH=$(cat ./.branch_deployed)
fi
echo "Branche de cet environnement : $ENV_BRANCH"
echo "Branche Bitbucket de référence pour les configs drupal : $DRUPAL_AUTO_UPDATE_ENV_REF"
if [ "$ENV_BRANCH" == "$DRUPAL_AUTO_UPDATE_ENV_REF" ] && [ "$DRUPAL_AUTO_UPDATE_ENV_REF" != "" ]; then
  echo "Cet environnement est l'environnement de référence. Contrôle des configs Drupal."
  drush_ps=$(./vendor/bin/drush banana_devops:checkForAvailableUpdate -y $drush_options|grep -v "warning"> /dev/stdout 2>&1)
  if [ "$?" -ne 0 ]; then
    echo "Erreur drush : $drush_ps"
  elif [[ "$drush_ps" =~ [0-9] ]]; then
    echo "Une mise à jour Drupal est disponible, lancement du step drupal-auto-update dans la branche $DRUPAL_AUTO_UPDATE_ENV_REF avec la varibale version définie à $drush_ps via la fonction lambda."
    curl -X POST -d '{"repository":"INSIGN_REPOSITORY_SLUG", "branch":"'$DRUPAL_AUTO_UPDATE_ENV_REF'", "step":"drupal-auto-update", "version":"'$drush_ps'"}' -H "x-api-key: AWS_LAMBDA_API_GATEWAY_KEY" -H "Content-Type: application/json" https://t4q3txq2lj.execute-api.eu-west-3.amazonaws.com/default/bitbucket-bot
  fi
fi

