#!/usr/bin/env bash
settings_file_path=$1
if [ -f $settings_file_path ]; then
  echo "Le fichier $settings_file_path existe déjà."
  exit 1
fi

cat << EOF > $settings_file_path
<?php
\$databases['default']['default'] = [
  'driver' => 'mysql',
  'namespace' => 'Drupal\Core\Database\Driver\mysql',
  'host' => '127.0.0.1',
  'port' => '3306',
  'database' => 'website',
  'username' => 'root',
  'password' => 'db_on_docker',
];
EOF