#!/usr/bin/env bash
#
#  Ce fichier provient du repository bitbucketci-drupal
#
export PLATFORMSH_CLI_NO_INTERACTION=1
platform ssh-cert:load
platform project:set-remote "$PF_PROJECT_ID"
ssh_co=$(platform ssh --environment=$DRUPAL_CONFIG_ENV_REF --pipe)

# Exports des configs sur l'environnement de référence
ssh $ssh_co bash -c "cd web; vendor/drush/drush/drush --root=/app/web/ cex"

sites="$(ls -d -1 web/sites/*/ | cut -d "/" -f3)"
# on récupère le répertoires de config défini dans chaque site
configs_all=()
for site in ${sites[@]}; do
  resp=$(ssh $ssh_co vendor/drush/drush/drush --root=/app/web/ -l $site core-status --field=config-sync)
  if [ "$?" -gt 0 ]; then
    echo "Une erreur est survenue lors de la connexion au serveur de $1 : $resp."
    echo "On arrête tout."
    exit 1
  fi
  if [ "$resp" != "" ]; then
    configs_all+=("web/"$resp)
  fi
done
# on dédoublonne le tableasu
configs=$(printf "%s\n" "${configs_all[@]}" | sort -u)
#echo "Debug $(IFS=" "; printf "%s" "${configs[*]}")"; exit 1
# on récupère le répertoires de config défini dans chaque site et les répertoires de config_split définis dans chaque config_split.config_split*
sync_made=""
for config in ${configs[@]}; do
  syncit="ok"
  if [ ! -d "$config" ]; then
    echo "Le répertoire de config $config n'existe pas, il va être synchronisé avec celui de $1".
    mkdir -p $config
  fi
  if [ "$syncit" == "ok" ]; then
    config=$(echo $config | sed -E "s|/$||")
    if [ "$config" != "" ]; then
      rsync -r --delete-after $ssh_co:./$config/ ./$config/
      #rsync -r $ssh_co:./$config/ ./$config/
      sync_made=$sync_made"$config "
    fi
  fi

  # configs_split_all=()
  # for config_split in $(find $config -name "config_split.config_split.*.yml" | rev | cut -d "/" -f1 | rev); do
  #   config_split=${config_split//.yml/}
  #   for site in ${sites[@]}; do
  #     configs_split_dir=$(ssh $ssh_co "vendor/drush/drush/drush --root=/app/web/ -l $site config:get $config_split 2>/dev/null |grep '^folder: ' |cut -d ":" -f2 |sed -E 's|^ ||'")
  #     if [ "$configs_split_dir" == "" ]; then
  #       continue;
  #     else
  #       configs_split_dir="web/"$configs_split_dir
  #     fi
  #     configs_split_dir_root=$(echo $configs_split_dir | sed -E "s|/$||")
  #     configs_split_dir_root=$(echo $configs_split_dir_root | sed -E "s|/[^/]+$||")
  #     configs_split_all+=($configs_split_dir_root)
  #   done
  # done
  # configs_split=$(printf "%s\n" "${configs_split_all[@]}" | sort -u)
  # for config_split in ${configs_split[@]}; do
  #   sync_split_it="ok"
  #   if [ ! -d "$config_split" ]; then
  #     echo "Le répertoire de config $config_split n'existe pas, il va être synchronisé avec celui de $1".
  #     mkdir -p $config_split
  #   fi
  #   if [ "$sync_split_it" == "ok" ]; then
  #     config_split=$(echo $config_split | sed -E "s|/$||")
  #     parent=$(echo $config_split | sed -E "s|/[^/]+$||")
  #     if [ "$config_split" == "" ]; then
  #       continue;
  #     else
  #       scp -r $ssh_co:./$config_split ./$parent
  #       sync_made=$sync_made"$config_split "
  #     fi
  #   fi
  # done
done

modified_files=$(git status -s $config/.)
if [ "$(echo "$modified_files" | grep -E '^[A ]?M')" ] || [ "$(echo "$modified_files" | grep -E '^[?]')" ]  || [ "$(echo "$modified_files" | grep -E '^ D')" ]; then
  echo "Configs à mettre à jour"
  # Si on est sur bitbucket
  if [ ! -z "$BITBUCKET_GIT_HTTP_ORIGIN" ]; then
    # Pour atteindre le repo à l'intérieur d'un pipe http://host.docker.internal:29418/
    # Pour atteindre le repo à l'intérieur d'une pipeline http://localhost:29418/
    git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://host.docker.internal:29418/
  fi
  git add $config/.
  git commit -m "Autocommit from bitbucket-bot"
  git push
else
  echo "Les configs Drupal de l'environnement de référence et celles de la branche courante sont identiques."
  echo "La commande git status -s $config/. a retourné : $modified_files"
fi
